<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Game;
use App\User;
use App\Genre;
use App\History;
use App\Pending;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('is_deleted', 0)
        ->where('user_role', 'customer')
        ->get();
        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
        return view('users.index', [
            'users' => $users,
            'requestTotal' => $requestTotal
        ]);
    }

    public function userIndex()
    {
        $pendings = Pending::where('user_id', Auth::user()->id)
            ->where('is_approved', 0)
            ->where('is_denied', 0)
            ->get();
        $borrowed_games = History::where('user_id', Auth::user()->id)
            ->get();
        $borrow_imagelocs = DB::table('histories')
            ->join('games', 'histories.game_id', '=', 'games.id')
            ->join('users', 'histories.user_id', '=', 'users.id')
            ->where('user_id', Auth::user()->id)
            ->where('is_borrowed', 1)
            ->get();
        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
        return view('users.userindex', [
            'pendings' => $pendings,
            'borrowed_games' => $borrowed_games,
            'borrow_imagelocs' => $borrow_imagelocs,
            'requestTotal' => $requestTotal
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function showRegister()
    {
        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
        return view('users.register', [
            'requestTotal' => $requestTotal
        ]);  
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'password' => 'required|min:8|confirmed',
        ]);

        $user = new user;

        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->contact_number = $request->contact_number;
        $user->password = Hash::make($request->password);
        $user->user_role  = 'customer';
        $user->save();

        $newUser = DB::table('users')
        ->orderBy('id', 'desc')
        ->first();

        $history = new History;
        $history->user_id = $newUser->id;
        $history->save();

        return redirect('/users/index')->with('alert-add-user', 'The user has been successfully registered.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $genres = Genre::all();
        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
        return view('users.edit', [
            'user' => $user,
            'requestTotal' => $requestTotal
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'password' => 'required|min:8|confirmed',
        ]);

        $user = user::find($id);

        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->contact_number = $request->contact_number;
        $user->password = Hash::make($request->password);

        $user->save();
        $request->session()->flash('message', 'The user has been updated.');

        return redirect('/users/index')->with('alert-edit-user', 'The user information has been successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);

        $user->is_deleted = 1;
        $user->save();

        $request->session()->flash('message', 'The user has been deleted.');

        return redirect('/users/index');
    }

    public function deleteConfirm($id)
    {
    $user = User::find($id);
    $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
    return view('users.delete', [
        'user' => $user,
        'requestTotal' => $requestTotal
    ]);
    }
}
