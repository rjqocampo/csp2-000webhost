<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Game;
use App\Genre;
use App\Pending;
use DB;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::where('is_archived', 0)
        ->where('is_available', 1)
        ->get();

        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();

        return view('games.index', [
            'games' => $games,
            'requestTotal' => $requestTotal
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genreOptions = Genre::all();
        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
        return view('games.create', [
            'genres' => $genreOptions,
            'requestTotal' => $requestTotal
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $game = new Game;

        $game->title = $request->title;
        $game->year = $request->year;
        $game->description = $request->description;
        $game->reviews = $request->reviews;
        $game->genre_id = $request->genre_id;
        $game->is_available = 1;

        $path = $request->image->store('images', ['disk' => 'public']);
        // $path = $request->file('image')->store('images/test.jpg', 'public');
        $game->image_location = $path;

        $game->save();

        return redirect('/menu')->with('alert-add-game', 'The game has been successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $game = Game::find($id);
        $genres = Genre::all();
        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
        return view('games.edit', [
            'game' => $game,
            'genres' => $genres,
            'requestTotal' => $requestTotal
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $game = Game::find($id);

        $game->title = $request->title;
        $game->year = $request->year;
        $game->description = $request->description;
        $game->reviews = $request->reviews;
        $game->genre_id = $request->genre_id;

        if ($request->hasFile('image')) {
            // remove the old file
            Storage::disk('public')->delete($game->image_location);
            // save the new image
            $path = $request->image->store('images', 'public');
            $game->image_location = $path;
        }

        $game->save();

        return redirect('/menu')->with('alert-edit-game', 'The game has been successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $game = Game::find($id);

        $game->is_archived = 1;
        $game->save();


        return redirect('/menu');
    }

    public function deleteConfirm($id)
    {
    $game = Game::find($id);
    $genres = Genre::find($game->genre_id);
    $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
    return view('games.delete', [
        'game' => $game,
        'genres' => $genres,
        'requestTotal' => $requestTotal
    ]);
    }
}
