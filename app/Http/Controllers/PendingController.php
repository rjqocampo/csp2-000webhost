<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Game;
use App\Genre;
use App\Pending;
use App\History;
use DB;

class PendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $game = Game::find($id);
        $genres = Genre::all();
        $check_borrows = History::all()
            ->where('user_id', Auth::user()->id)
            ->sortByDesc('borrow_date')
            ->first();

        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
        return view('pending.index', [
            'game' => $game,
            'genres' => $genres,
            'check_borrows' => $check_borrows,
            'requestTotal' => $requestTotal
        ]);

    }

    public function adminIndex()
    {
        $pendings = Pending::all();
        $games = Game::all();
        $histories = History::all();
        $requestTotal = Pending::where('is_approved', 0)
        ->where('is_denied', 0)
        ->count();
        return view('pending.adminindex', [
            'pendings' => $pendings,
            'games' => $games,
            'histories' => $histories,
            'requestTotal' => $requestTotal
        ]);
    }

    public function isApproved($id)
    {
        $pending = Pending::find($id);
        $pending->is_approved = 1;
        $pending->is_denied = 0;
        $pending->save();

        $game = Game::find($pending->game_id);
        $game->is_available = 0;
        $game->save();

        $timestamp = now();

        $history = new History;
        $history->is_borrowed = 1;
        $history->is_returned = 0;
        $history->borrow_date = $timestamp;
        $history->game_id = $pending->game_id;
        $history->user_id = $pending->user_id;
        $history->save();

        return redirect('pending/requests');
    }

    public function isDenied($id)
    {
        $pending = Pending::find($id);

        $pending->is_approved = 0;
        $pending->is_denied = 1;
        $pending->save();

        return redirect()->back()->with('alert', 'Deleted!');
    }

    public function isReturned($id)
    {
        $timestamp = now();

        $history = History::find($id);
        $history->is_borrowed = 0;
        $history->is_returned = 1;
        $history->return_date = $timestamp;
        $history->save();

        $game = Game::find($history->game_id);
        $game->is_available = 1;
        $game->save();

        return redirect('pending/requests');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $pending = new Pending;
        $pending->user_id = Auth::user()->id;
        $pending->game_id = $id;
        $pending->save();

        return redirect('users/history');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
