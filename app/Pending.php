<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pending extends Model
{
	public $timestamps = false;

    public function user() {
    	return $this->belongsTo("App\User");
    }

    public function game() {
    	return $this->belongsTo("App\Game");
    }
}
