@if(!empty(Auth::user()) && Auth::user()->is_deleted == 1)
    <script>window.location = '/deleted'</script>
@endif

@extends('layouts.app')

@section('title', 'Menu')

@section('menu')
<div class="row justify-content-center pb-5 content-box">
    
    <div class="col-md-12">

        @if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')
        <div class="col-7 card bg-dark mt-5 p-0 admin-card">
            <div class="card-header">
                <text class=" admin-header">Administrator</text>
            </div>

            <div class="card-body">
                @if(session('alert-add-game'))
                <div class="col-6 alert alert-primary mx-1 px-1">
                    <small><i class="fas fa-exclamation-circle fa-lg p-2"></i>{{ session()->get('alert-add-game') }}</small>
                </div>
                @elseif(session('alert-edit-game'))
                <div class="col-6 alert alert-primary mx-1 px-1">
                    <small><i class="fas fa-exclamation-circle fa-lg p-2"></i>{{ session()->get('alert-edit-game') }}</small>
                </div>
                @endif
                
                <a href="{{ url('games/create') }}" class="btn peach-gradient admin-button">Add Game</a>
                <a href="{{ url('users/register') }}" class="btn peach-gradient admin-button">Add User</a>
            </div>
        </div>
        
        <br>
        @endif
        
        <h3 class="menu-header">
            <label>Discover</label>
        </h3>
        
        <div class="card landing-menu-box">
            <div class="card-body landing-card-box py-0">
                <div class="row">
                    @foreach ($games as $game)
                        <div class="col-3 mt-3 d-flex align-items-stretch landing-card-container">
                            <div class="card landing-card">
                                @if($game->is_available == 1)
                                <img src='{{ asset("$game->image_location") }}' class="img-fluid">

                                <div class="card-body landing-card-body">
                                    <h4 class="card-title landing-card-title">{{ $game->title }}</h4>

                                    <p class="card-text landing-card-text">Summary: {{ $game->description }}</p>
                                    <p class="card-text landing-card-text">Genre: {{ $game->genre->name }}</p>
                                </div>
                                @endif

                                @if (!empty(Auth::user()))

                                @if (Auth::user()->user_role == "admin")
                                <div class="card-footer landing-card-footer btn-group btn-block">
                                    <a class="btn btn-info px-2 admin-card-button" href='{{ url("games/$game->id/edit") }}'>Edit</a>
                                    <a class="btn btn-danger px-2 admin-card-button" href='{{ url("/games/$game->id/delete-confirm") }}'>Delete</a>
                                </div>
                                @elseif (Auth::user()->user_role == "customer")
                                <div class="card-footer landing-card-footer btn-group btn-block">
                                    <form action='{{ url("/pending/$game->id/index" )}}'class="form-add-to-cart" data-id="{{ $game->id }}">
                                        <div class="btn-group btn-block">
                                            <button class="btn landing-card-button peach-gradient">Submit Request</button>
                                        </div>
                                    </form>
                                </div>
                                @endif

                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>

@endsection

@section('landing-page')
    <div class="container-fluid landing-page">
        <div class="row no-gutters">
            <div class="col-6 landing-page-col1">
                <div class="landing-page-content">
                    <h1>Your go-to-source for video games!</h1>
                    <h3>Visit our store and join.</h3>
                    <ul>
                        <li><img class="svg-zap" src='{{ asset("/img/zap.svg") }}'>Get access to a library of both latest and classic games</li>
                        <li><img class="svg-zap" src='{{ asset("/img/zap.svg") }}'>Have your games delivered right to your mailbox</li>
                        <li><img class="svg-zap" src='{{ asset("/img/zap.svg") }}'>If you enjoy it, you can keep it for a cheap price</li>
                        <li><img class="svg-zap" src='{{ asset("/img/zap.svg") }}'>No due dates or late fees</li>
                    </ul>
                    <div class="login-container">
                        <text>
                            Already have an account? 
                        </text>
                        <form action="{{ route('login') }}">
                            <button type="submit" class="btn btn-default">
                                <a class="" id="navbar-login">{{ __('Login') }}</a>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-6 landing-page-col2">
                <div class="floating1">
                    <img id="png-controller" src='{{ asset("/img/controller2.png") }}'>
                </div>
                <div class="floating2">
                    <img id="png-gameboy" src='{{ asset("/img/gameboy5.png") }}'>
                </div>
            </div>
        </div>
        
        <img class="svg-wave2" src="{{url('/img/wave2.svg')}}" alt="">
    </div>
@endsection

@section('content')
    @if (empty(Auth::user()))
        @yield('landing-page')
    @endif
    <div class="container-fluid">
        @yield('menu')
    <div class="container-fluid">
@endsection