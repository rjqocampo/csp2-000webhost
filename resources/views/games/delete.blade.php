@if(Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Delete Game')

@section('delete-game-form')

	<form action='{{ url("games/$game->id") }}' method="post" enctype="multipart/form-data">
		@csrf
		@method("DELETE")

		<div class="form-group">
			<label>Title</label>
			<input type="text" class="form-control" value="{{ $game->title }}" readonly>
		</div>

		<button type="submit" class="btn btn-danger btn-block">
			Delete	
		</button>
		
	</form>

@endsection

@section('content')

	<div class="container-fluid content-box content-box">
		<div class="row">
			<div class="col-4 mx-auto">
				<div class="card">
					<div class="card-header">
						Delete Game Confirmation
					</div>

					<div class="card-body">
						@yield('delete-game-form')
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection