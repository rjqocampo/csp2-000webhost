@if(Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Edit Game')

@section('edit-item-form')

<div class="row">
	<div class="col-5 mx-auto">
		<div class="card">

			<div class="card-header">
				Edit Game Information
			</div>

			<div class="card-body">
				<form action='{{ url("games/$game->id") }}' method="post" enctype="multipart/form-data">
					@csrf
					@method("PUT")
					<div class="row d-flex justify-content-center">
						<div class="col-5 md-form form-group">
							<label>Title</label>
							<input type="text" class="form-control" value="{{ $game->title }}" name="title" required>
						</div>

						<div class="col-5 md-form form-group">
							<label>Year</label>
							<input type="text" class="form-control" value="{{ $game->year }}" name="year">
						</div>
					</div>

					<div class="md-form form-group">
						<label>Description</label>
						<textarea type="text" class="md-textarea form-control" name="description" required>{{ $game->description }}</textarea>
					</div>

					<div class="md-form form-group">
						<label>Reviews</label>
						<textarea type="text" class="md-textarea form-control" name="reviews" required>{{ $game->reviews }}</textarea>
					</div>

					<div>
						<label>Genre</label>
						<select class="form-control" name="genre_id" required>
							<option value selected disabled>Select Genre</option>
							@foreach ($genres as $genre)
								@if ($genre->id == $genre->id)
									<option value="{{ $genre->id }}">{{ $genre->name }}</option>
								@else
									<option value="{{ $genre->id }}">{{ $genre->name }}</option>
								@endif
							@endforeach	
							</option>
						</select>
					</div> 

					<div class="form-group mt-2">
						<label>Cover Art</label>
						<input class="btn" type="file" class="form-control" name="image">
					</div>

					<button type="submit" class="btn blue-gradient btn-success btn-block">Save Edit</button>
				</form>
			</div>
			
		</div>
	</div>
</div>

@endsection

@section('content')
	<div class="container-fluid content-box pb-4 content-box">
		@yield('edit-item-form')
	</div>
@endsection