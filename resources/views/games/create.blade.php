@extends('layouts.app')

@section('title', 'Add Game')

@section('add-item-form')

	<form action="{{ url('games/store') }}" method="post" enctype="multipart/form-data">
		@csrf

		<div class="row d-flex justify-content-center">
			<div class="col-5 md-form form-group">
				<label>Title</label>
				<input type="text" class="form-control" name="title" required>
			</div>

			<div class="col-5 md-form form-group">
				<label>Year</label>
				<input type="text" class="form-control" name="year">
			</div>
		</div>

		<div class="md-form form-group">
			<label>Description</label>
			<textarea type="text" class="md-textarea form-control" name="description" required></textarea>
		</div>

		<div class="md-form form-group">
			<label>Reviews</label>
			<textarea type="text" class="md-textarea form-control" name="reviews" required></textarea>
		</div>

		<div>
			<select class="form-control" name="genre_id">
				<option value selected disabled>Select Genre</option>
				@foreach ($genres as $genre)
					<option value="{{ $genre->id }}">{{ $genre->name }}</option>
				@endforeach	
				</option>
			</select>
		</div>

		<br>

		<div class="form-group">
			<label>Cover Art</label>
			<input class="btn" type="file" class="form-control" name="image" required>
		</div>

		<button type="submit" class="btn blue-gradient btn-success btn-block">Add</button>
	</form>

@endsection

@section('content')

	<div class="container-fluid content-box content-box">
		<divclass="row">
			<div class="col-5 mx-auto">
				<div class="card">
					<div class="card-header">Item Information
					</div>

					<div class="card-body">
						@yield('add-item-form')
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection