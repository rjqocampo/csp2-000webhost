@if(Auth::user()->is_deleted == 1)
    <script>window.location = '/deleted'</script>
@endif

@extends('layouts.app')

@section('title', 'Pending Requests')

@section('content')

<div class="container-fluid content-box">
    <div class="row justify-content-center">
        <div class="col-md-3 mb-3">
            <div class="card">
                <div class="card-header">
                    Current Borrowed Game
                </div>

                <div class="card-body p-2">
                    @foreach($borrow_imagelocs as $imageloc)   
                        <img src='{{ asset("storage/$imageloc->image_location") }}' class="img-fluid">

                        <h5 class="card-title mt-3 text-center">{{ $imageloc->title }}</h5>
                    @endforeach
                </div>

                <div class="col-12 d-flex justify-content-center">
                    <small class="alert alert-info p-3 user-list-alert"><i class="fas fa-exclamation-circle fa-lg p-1"></i>Only one game can be borrowed per user.</small>
                </div>
            </div>
        </div>

        <div class="col-md-4 mb-3">
            <div class="card">
                <div class="card-header">
                    Requests
                </div>

                <table class="table">
                    <tr>
                        <th>Request #</th>
                        <th>Game</th>
                        <th>Status</th>
                    </tr>

                    @foreach($pendings as $pending)
                    <tr>
                        <td>#{{ $pending->id }}</td>
                        <td>{{ $pending->game->title}}</td>
                        <td class="text-warning"><strong>Pending</strong></td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div class="col-md-4 mb-3">
            <div class="card">
                <div class="card-header">
                    History
                </div>

                <table class="table">
                    <tr>
                        <th>Game</th>
                        <th>Borrow Date</th>
                        <th>Return Date</th>
                    </tr>

                    @foreach($borrowed_games as $borrowed)
                    @if($borrowed->is_returned == 1)
                    <tr>
                        <td>{{ $borrowed->game->title }}</td>
                        <td>{{ $borrowed->borrow_date }}</td>
                        <td>{{ $borrowed->return_date }}</td>
                    </tr>
                    @endif
                    @endforeach
                </table>
            </div>
        </div>

    </div>
</div>

@endsection

