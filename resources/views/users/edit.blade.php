@extends('layouts.app')

@section('title', 'Edit User')

@section('content')

<div class="container content-box">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">

				<div class="card-header">
					Edit User Information
				</div>

				<div class="card-body">
					<form action='{{ url("users/$user->id") }}' method="post" enctype="multipart/form-data">
						@csrf
						@method("PUT")

						<div class="form-group form-control-sm row">
							<label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
							<div class="col-md-6">
								<input id="name" type="text" class="form-control" value="{{ $user->name }}" name="name">
							</div>
						</div>

						<div class="form-group form-control-sm row">
							<label for="username" class="col-md-4 col-form-label text-md-right">Username</label>
							<div class="col-md-6">
								<input id="username" type="text" class="form-control" value="{{ $user->username }}" name="username">
							</div>
						</div>

						<div class="form-group form-control-sm row">
							<label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
							<div class="col-md-6">
								<input id="email" type="email" class="form-control" value="{{ $user->email }}" name="email">
							</div>
						</div>

						<div class="form-group form-control-sm row">
							<label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
							<div class="col-md-6">
								<input id="address" type="address" class="form-control" value="{{ $user->address }}" name="address">
							</div>
						</div>

						<div class="form-group form-control-sm row">
							<label for="contact_number" class="col-md-4 col-form-label text-md-right">Contact Number</label>
							<div class="col-md-6">
								<input id="contact_number" type="contact_number" class="form-control" value="{{ $user->contact_number }}" name="contact_number">
							</div>
						</div>

						<div class="form-group form-control-sm row pb-5">
						    <label for="password" class="col-md-4 col-form-label text-md-right">Enter New Password</label>

						    <div class="col-md-6">
						        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">

						        @error('password')
						            <span class="invalid-feedback" role="alert">
						                <strong>{{ $message }}</strong>
						            </span>
						        @enderror
						    </div>
						</div>

						<div class="form-group form-control-sm row pb-5">
						    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm New Password</label>

						    <div class="col-md-6">
						        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
						    </div>
						</div>

						<div class="form-group row mb-0">
						    <div class="col-md-6 offset-md-4">
								<button type="submit" class="btn blue-gradient btn-primary">Save Edit</button>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>




@endsection
