@extends('layouts.app')

@section('title', 'Delete Game')

@section('delete-game-form')

		<div class="col-6 alert alert-danger mx-1 px-1">
		    <small><i class="fas fa-exclamation-circle fa-lg p-2"></i>User is deleted.</small>
		</div>

		<a href='{{ url("deleted/route") }}' type="submit" class="btn btn-danger btn-block">
			Return to Login Page	
		</a>
		
@endsection

@section('content')

	<div class="container-fluid content-box">
		<div class="row">
			<div class="col-4 mx-auto">
				<div class="card">
					<div class="card-header">
						Delete Game Confirmation
					</div>

					<div class="card-body">
						@yield('delete-game-form')
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection