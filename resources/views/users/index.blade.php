@extends('layouts.app')

@section('title', 'Users')

@section('content')

<div class="container-fluid content-box">
    <div class="row justify-content-center">

        @if(session('alert-add-user'))
        <div class="col-3 alert alert-primary mx-1 px-1 justify-content-left">
            <small><i class="fas fa-exclamation-circle fa-lg p-2"></i>{{ session()->get('alert-add-user') }}</small>
        </div>
        @endif

        @if(session('alert-edit-user'))
        <div class="col-3 alert alert-primary mx-1 px-1 justify-content-left">
            <small><i class="fas fa-exclamation-circle fa-lg p-2"></i>{{ session()->get('alert-edit-user') }}</small>
        </div>
        @endif

        <div class="col-md-11">
            <div class="card">
                <div class="card-header">Users</div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-sm">
                        <tr>
                            <th>Username</th>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Address</th>
                            <th>Contact Number</th>
                            <th>Actions</th>
                        </tr>

                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->username }}</h4>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->address }}</td>
                            <td>{{ $user->contact_number }}</td>
                            @if (Auth::user()->user_role == "admin")
                            <td>
                                <a class="btn btn-sm blue-gradient" href='{{ url("/users/$user->id/edit") }}'>Edit</a>
                                <a class="btn btn-sm btn-danger" href='{{ url("/users/$user->id/delete-confirm") }}'>Delete</a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection