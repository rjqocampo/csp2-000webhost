@if(Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Delete User')

@section('delete-user-form')

	<form action='{{ url("users/$user->id") }}' method="post" enctype="multipart/form-data">
		@csrf
		@method("DELETE")

		<div class="form-group">
			<label>Username</label>
			<input type="text" class="form-control" value="{{ $user->username }}" readonly>
		</div>

		<div class="form-group">
			<label>Name</label>
			<input type="text" class="form-control" value="{{ $user->name }}" readonly>
		</div>

		<button type="submit" class="btn btn-danger btn-block">
			Delete	
		</button>
		
	</form>

@endsection

@section('content')

	<div class="container-fluid content-box">
		<div class="row">
			<div class="col-6 mx-auto">
				<h3 class="text-center">
					Delete User
				</h3>

				<div class="card">
					<div class="card-header">
						User Information
					</div>

					<div class="card-body">
						@yield('delete-user-form')
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection