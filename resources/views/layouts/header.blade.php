<nav class="navbar fixed-top navbar-expand-lg shadow-none navbar-dark scrolling-navbar">
    <div class="container-fluid nav-box mx-0">
        <a class="navbar-brand" id="gamebox-brand" href="{{ url('/menu') }}">
            <i class="fas fa-cube fa-1x p-1 brand-icon"></i><text class="brand-one">Game</text><text class="brand-two">BOX</text>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link navbar-link" id="navbar-login" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        @if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')
                        <a href="{{ url('/menu') }}" class="nav-link d-inline navbar-link">home</a>
                        <a href="{{ url('users/index') }}" class="nav-link d-inline navbar-link">users</a>
                        <a href="{{ url('pending/requests') }}" class="nav-link d-inline navbar-link">requests</a><span class="text-center request-total">{{ $requestTotal }}</span>
                        @endif
                        <a id="navbarDropdown" class="nav-link dropdown-toggle d-inline navbar-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if (!empty(Auth::user()) && Auth::user()->user_role == 'customer')
                            <a href="{{ url('/menu') }}" class="dropdown-item drop-link">Home</a>
                            <a href="{{ url('users/history') }}" class="dropdown-item drop-link">My Request History</a>
                            @endif

                            <a class="dropdown-item drop-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>

                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>