<footer id="main-footer" class="bg-dark">
	<div>
		<div class="row no-gutters justify-content-md-center footer1">
			<div class="col-12">
				<img class="svgWave" src="{{url('/img/wave1.svg')}}" alt="">
			</div>
			<div class="col-4">
				<h1>Follow us</h1>
				<ul>
					<a href="#"><i class="fab fa-facebook fa-5x"></i></a>
					<a href="#"><i class="fab fa-twitter fa-5x"></i></a>
					<a href="#"><i class="fab fa-instagram fa-5x"></i></a>
					<a href="#"><i class="fab fa-youtube fa-5x"></i></a>
				</ul>
				<h4>Sign up to our newslettter.</h4>
				<form action="#">
					<input type="text" id="exampleForm2" class="form-control-sm" placeholder="Your email">
					<button type="button" class="btn btn-default btn-sm">SIGN UP</button>
				</form>
			</div>
			<a href="#top"><i class="fas fa-arrow-up fa-3x"></i></a>
		</div>
		<div class="row no-gutters footer2">
			<div class="col-3">
				<h6 id="footerLabel">NAVIGATE</h6>
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Careers</a></li>
					<li><a href="#">Blog</a></li>
				</ul>
				<h6 id="footerLabel">LEGAL</h6>
				<ul>
					<li><a href="#">Terms and Conditions</a></li>
					<li><a href="#">Privacy and Policy</a></li>
				</ul>
			</div>
			<div class="col-3">
				<h6 id="footerLabel">CONTACTS</h6>
				<ul>
					<li>+987 654 3210</li>
					<li>gamebox@example.com</li>
					<br>
					<li>EDSA corner Timog Avenue, Quezon City, Metro Manila, Philippines</li>
					<br>
					<li>&copy; 2020 GameBOX</li>
				</ul>
			</div>
			<div class="col-6">
				<iframe class="googleMap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.41005546066!2d121.0418374430264!3d14.632649457388657!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b7afe0500001%3A0x7cc8625aea7639f0!2sTuitt%20Coding%20Bootcamp!5e0!3m2!1sen!2sph!4v1571311826810!5m2!1sen!2sph" height="100%" width="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
	</div>
</footer>