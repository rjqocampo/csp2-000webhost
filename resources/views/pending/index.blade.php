@extends('layouts.app')

@section('title', 'Request Game')

@section('request-game-form')
	<div class="row justify-content-center">
	    <div class="col-md-10">
	        <div class="card">
				<div class="card-header blue-gradient confirm-card-header">
					Confirm Request
				</div>

				<div class="card-body confirm-card-body">
					<form action='{{ url("pending/$game->id") }}' method="post" enctype="multipart/form-data">
						@csrf
						@method("PUT")

						<div class="row">
						    <div class="col-md-7">
								<img src='{{ asset("storage/$game->image_location") }}' class="card-img-left img-fluid">
							</div>
						
							<div class="col-md-5 d-flex flex-column pt-1" style="font-size: .7rem;">
								<div class="form-group confirm-card-title">
									<h4 name="title">{{ $game->title }}</h4>
								</div>

								<div class="form-group">
									<p class="card-text confirm-card-text">Summary: {{ $game->description }}</p>
								</div>

								<div class="form-group">
									<p class="card-text confirm-card-text">Year Released: {{ $game->year }}</p>
								</div>

								<div>
									<p class="card-text confirm-card-text">Genre: {{ $game->genre->name }}</p>
								</div> 

								<br>

								<div class="form-group">
									<label name="reviews" class="confirm-card-text">Reviews:</label>
									<p class="confirm-card-text"><i>{{ $game->reviews }}</i></p>
								</div>

								<p class="alert alert-info"><i class="fas fa-exclamation-circle fa-lg p-2 confirm-card-notice"></i>Only one game can be borrowed per user.</p>

								@if($check_borrows->is_borrowed == 1)
								<p class="alert alert-danger"><i class="fas fa-exclamation-circle fa-lg p-2 confirm-card-notice"></i>You already have a borrowed game.</p>
								@elseif($check_borrows->is_returned == 1)
								<div>
									<button type="submit" class="btn peach-gradient btn-block confirm-card-button">Submit</button>
								</div>
								@else
								
								<div>
									<button type="submit" class="btn peach-gradient btn-block confirm-card-button">Submit</button>
								</div>
								@endif


							</div>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('content')
	<div class="container content-box">
		@yield('request-game-form')
	</div>
@endsection