@extends('layouts.app')

@section('title', 'Request Game')

@section('request-game-form')

<form action='{{ url("games/$game->id") }}' method="post" enctype="multipart/form-data">
	@csrf
	@method("PUT")

	<img src='{{ asset("storage/$game->image_location") }}' class="card-img-top" width="100%" height="200px">

	<div class="form-group">
		<label>Cover</label>
		<input type="file" class="form-control" name="image">
	</div>

	<div class="form-group">
		<label>Title</label>
		<input type="text" class="form-control" value="{{ $game->title }}" name="title" required>
	</div>

	<div class="form-group">
		<label>Year</label>
		<input type="text" class="form-control" value="{{ $game->year }}" name="year">
	</div>

	<div class="form-group">
		<label>Reviews</label>
		<input type="text" class="form-control" value="{{ $game->reviews }}" name="reviews" required>
	</div>

	<div>
		<label>Genre</label>
		<select class="form-control" name="genre_id">
			<option value selected disabled>Select Genre</option>
			@foreach ($genres as $genre)
				@if ($genre->id == $genre->id)
					<option value="{{ $genre->id }}" selected>{{ $genre->name }}</option>
				@else
					<option value="{{ $genre->id }}">{{ $genre->name }}</option>
				@endif
			@endforeach	
			</option>
		</select>
	</div> 

	<button type="submit" class="btn btn-success btn-block">Save Edit</button>
</form>

@endsection

@section('content')
	<div class="container-fluid content-box">
		<div class="row">
			<div class="col-6 mx-auto">
				<h3 class="text-center">Request Page</h3>
				<div class="card">
					<div class="card-header">Game Information</div>
					<div class="card-body">
						@yield('request-game-form')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection