@extends('layouts.app')

@section('title', 'Pending Requests')

@section('content')

<div class="container-fluid content-box pb-5">
    <div class="row justify-content-center">
        <div class="col-md-10">
            
            <div class="card">
                <div class="card-header">
                    Requests
                </div>

                <div class="card-body">
                    <table class="table table-sm">
                        <tr>
                            <th>Request #</th>
                            <th>Name</th>
                            <th>Game Title</th>
                            <th>Actions</th>
                            <th>Status</th>
                        </tr>

                        @foreach ($pendings as $pending)
                        <tr>
                            <td>#{{ $pending->id }}</td>
                            <td>{{ $pending->user->name }}</td>
                            <td>{{ $pending->game->title }}</td>
                            <td class="m-0 p-1">
                                <div class="row d-flex justify-content-left">
                                @if($pending->is_approved == 0 AND $pending->is_denied == 0)
                                <div class="col-5">
                                    <form action='{{ url("/pending/$pending->id/approved") }}' method="post" enctype="multipart/form-data" class="btn-group btn-block">
                                        @csrf
                                        @method("PUT")
                                        <button type="submit" class="btn blue-gradient btn-sm p-1 admin-list-button"><small>Approve</small></button>
                                    </form>
                                </div>

                                <div class="col-5">
                                    <form action='{{ url("/pending/$pending->id/denied" )}}' method="post" enctype="multipart/form-data" class="btn-group btn-block">
                                        @csrf
                                        @method("PUT")
                                        <button class="btn btn-danger btn-sm p-1 admin-list-button"><small>Deny</small></button>
                                    </form>

                                @elseif($pending->is_approved == 1 OR $pending->is_denied == 1)
                                <text class="pt-1">Processed</text>
                                @endif
                                </div>
                            </td>

                            @if($pending->is_approved == 1)
                            <td class="text-success"><strong>Approved</strong></td>
                            @elseif($pending->is_denied == 1)
                            <td class="text-danger"><strong>Denied</strong></td>
                            @else
                            <td class="text-warning"><strong>Pending</strong></td>
                            @endif
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>

            <br>

            <div class="card">
                <div class="card-header">
                    Borrowed List
                </div>

                <div class="card-body">
                    <table class="table table-sm">
                        <tr>
                            <th>Username</th>
                            <th>Name</th>
                            <th>Borrowed Game</th>
                            <th>Borrow Date</th>
                            <th>Return Date</th>
                            <th>Tag as Returned</th>
                            <th>Status</th>
                        </tr>

                        @foreach ($histories as $history)
                        @if($history->is_borrowed == 1 OR $history->is_returned == 1)
                        <tr>
                            <td>{{ $history->user->username }}</td>
                            <td>{{ $history->user->name }}</td>
                            <td>{{ $history->game->title }}</td>
                            <td>{{ $history->borrow_date }}</td>

                            @if($history->borrow_date == $history->return_date)
                            <td>Not Returned</td>
                            @else
                            <td>{{ $history->return_date }}</td>
                            @endif

                            @if($history->is_returned == 1)
                            <td>Processed</td>
                            @else
                            <td class="m-0 p-1">
                                <form action='{{ url("/pending/$history->id/returned") }}' method="post" enctype="multipart/form-data" class="btn-group btn-block">
                                    @csrf
                                    @method("PUT")
                                    <button type="submit" class="btn btn-sm blue-gradient p-1 admin-list-button">Confirm</button>
                                </form>
                            </td>
                            @endif

                            @if($history->is_borrowed == 1)
                            <td class="text-warning"><strong>Borrowed</strong></td>
                            @elseif($history->is_returned == 1)
                            <td class="text-success"><strong>Returned</strong></td>
                            @else
                            <td class="text-warning"><strong>null</strong></td>
                            @endif
                        </tr>
                        @endif
                        @endforeach
                    </table>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>

@endsection