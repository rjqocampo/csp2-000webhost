<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/menu', 'GameController@index');
Route::get('/games/create', 'GameController@create');
Route::post('/games/store', 'GameController@store');
Route::get('/games/{id}/edit', 'GameController@edit');
Route::put('/games/{id}', 'GameController@update');
Route::get('/games/{id}/delete-confirm', 'GameController@deleteConfirm');
Route::delete('/games/{id}', 'GameController@destroy');

Route::get('/users/register', 'UserController@showRegister');
Route::post('/users/store', 'UserController@store');
Route::get('/users/index', 'UserController@index');
Route::get('/users/{id}/edit', 'UserController@edit');
Route::put('/users/{id}', 'UserController@update');
Route::get('/users/{id}/delete-confirm', 'UserController@deleteConfirm');
Route::delete('/users/{id}', 'UserController@destroy');
Route::get('/users/history', 'UserController@userIndex');

Route::get('/pending/{id}/index', 'PendingController@index');
Route::put('/pending/{id}', 'PendingController@store');

Route::get('/pending/requests', 'PendingController@adminIndex');
Route::put('/pending/{id}/approved', 'PendingController@isApproved');
Route::put('/pending/{id}/denied', 'PendingController@isDenied');
Route::put('/pending/{id}/returned', 'PendingController@isReturned');

Route::get('/logout', 'HomeController@index');
Route::get('/deleted', 'HomeController@deleted');
Route::get('/deleted/route', 'HomeController@deletedRoute');